namespace Andromeda.AvaloniaApp.Helpers {
    public class NotificationData {
        public string Message { get; set; }

        public NotificationData(string message) {
            this.Message = message;
        }
    }
}
