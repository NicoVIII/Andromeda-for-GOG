# Changelog of Andromeda for GOG

## v0.3.0 (Alpha)

-   a first very basic GUI (for now without browser with workaround buttons) which can install games, update games and start (some) games
-   caches installers and reuses already downloaded files
-   uses "unzip" on linux systems for some installers, which otherwise wouldn't install (e.g. State of Mind)
-   internal code restructuring
-   improve handling of aborted downloads

## v0.2.0

-   add automatic downloading and installing of games
-   add ability to automatically upgrade installed games

## v0.1.0

initial, very basic version with console interface
